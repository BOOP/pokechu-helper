from discord.ext import commands
from utilities import suggestions_handler, levels_handler
from os import environ, listdir
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())
import discord

client = commands.Bot(command_prefix='~')
client.remove_command('help')

@client.event
async def on_ready():
    await client.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name="you."))
    print(f'Bot has been loaded ({client.user})')

@client.event
async def on_command_error(ctx, error):
    if isinstance(error, commands.MissingPermissions) or isinstance(error, commands.CheckFailure):
        embed = discord.Embed(title='No Permission', description='Sorry, you lack the required permissions for this command.', color=0xfc4144)
        
        await ctx.send(embed=embed)
    elif isinstance(error, commands.MissingRequiredArgument):
        embed = discord.embed(title='Invalid Arguments', description='It appears you did not give valid arguments. Please try again.')

        await ctx.send(embed=embed)
    else:
        await ctx.send('An error was encountered, please tag BOOP and let me know what happened.')
        print(error)

categories = ['leveling', 'suggestions', 'moderation', 'miscellaneous']
for filename in listdir('./cogs'):
    if filename.endswith('.py'):
        try:
            client.load_extension(f'cogs.{filename[:-3]}')
            print(f'Loaded {filename[:-3]} cog...')
        except:
            print(f'Unable to load {filename[:-3]}..')

for cat in categories:
    for filename in listdir(f'./cogs/{cat}'):
        if filename.endswith('.py'):
            try:
                client.load_extension(f'cogs.{cat}.{filename[:-3]}')
                print(f'Loaded {filename[:-3]} cog...')
            except Exception as e:
                print(e)
                print(f'Unable to load {filename[:-3]}...')

client.run(environ.get('BOT_TOKEN'))