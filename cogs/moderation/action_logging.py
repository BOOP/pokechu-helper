from discord.ext import commands
from utilities.db_handler import infractions as infr
from utilities import moderation_handler as moderation
from datetime import datetime
import discord

class action_logging(commands.Cog):
    @commands.Cog.listener()
    async def on_message_delete(self, msg:discord.Message):
        if msg.author.id in [677001237408907265, 721582760413757453]: return
        log_channel = discord.utils.get(msg.guild.channels, name='message-log')

        embed = discord.Embed(title='Message Deleted', description=msg.content)
        embed.set_author(name=msg.author.name, icon_url=msg.author.avatar_url)
        embed.set_footer(text=f'Time: {datetime.utcnow().strftime("%b %d %Y, %I:%M:%S %p")} | Author ID: {msg.author.id}')

        if len(msg.attachments) > 0:
            for a in msg.attachments:
                embed.add_field(name='Attachment', value=a.proxy_url)
        await log_channel.send(embed=embed)        

    @commands.Cog.listener()
    async def on_message_edit(self, msg:discord.Message, msg2:discord.Message):
        if msg.author.id in [677001237408907265, 721582760413757453]: return
        log_channel = discord.utils.get(msg.guild.channels, name='message-log')
        
        embed = discord.Embed(title='Message Edited', description=f'A message by {msg.author.name} has been edited.')
        embed.add_field(name='Old Message', value=str(msg.content), inline=True)
        embed.add_field(name='New Message', value=str(msg2.content), inline=True)
        embed.set_author(name=msg.author.name, icon_url=msg.author.avatar_url)
        embed.set_footer(text=f'Time: {datetime.utcnow().strftime("%b %d %Y, %I:%M:%S %p")} | Author ID: {msg.author.id}')

        await log_channel.send(embed=embed)

    def __init__(self, client):
        self.client : commands.Bot = client
def setup(client):
    client.add_cog(action_logging(client)) 