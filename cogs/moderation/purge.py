from discord.ext import commands
from utilities.db_handler import infractions as infr
from utilities import moderation_handler as moderation
import discord
import time

class purge(commands.Cog):
    @commands.command()
    @commands.has_any_role('Admin', 'Staff')
    async def purge(self, ctx : commands.Context, limit=15, *, reason=None):
        await ctx.message.delete()
        deleted = await moderation.purge_messages(ctx, limit, reason=reason)
        await ctx.send(f'Successfully deleted {len(deleted)} messages.', delete_after=5.0)

    def __init__(self, client):
        self.client : commands.Bot = client
def setup(client):
    client.add_cog(purge(client))