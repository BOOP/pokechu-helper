from discord.ext import commands
from utilities.db_handler import infractions as infr
from utilities import moderation_handler as moderation
import discord

class ban(commands.Cog):
    @commands.command()
    @commands.bot_has_permissions(ban_members=True)
    @commands.has_any_role('Staff', 'Admin')
    async def ban(self, ctx, user, *, ban_reason):
        if len(ctx.message.mentions) > 1 or len(ctx.message.mentions) < 1: await ctx.send('Invalid arguments (not enough or too many mentions)'); return
        target = ctx.guild.get_member(ctx.message.mentions[0].id)

        await moderation.ban_user(ctx, target, reason=ban_reason)
        
    def __init__(self, client):
        self.client : commands.Bot = client
def setup(client):
    client.add_cog(ban(client)) 