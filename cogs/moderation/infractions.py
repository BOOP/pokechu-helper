from discord.ext import commands
from utilities.db_handler import infractions as infr
from utilities.db_handler import db
from utilities import moderation_handler as moderation
import discord

class infractions(commands.Cog):
    @commands.command(aliases=['history'])
    @commands.has_any_role('Staff', 'Admin')
    async def infractions(self, ctx, user):
        if len(ctx.message.mentions) > 1 or len(ctx.message.mentions) < 1: await ctx.send('Invalid arguments (not enough or too many mentions)'); return
        target = ctx.guild.get_member(ctx.message.mentions[0].id)
        if not moderation.check_user_id(target.id): await ctx.send(f'{target.name} currently has no infractions.'); return

        embed = discord .Embed(title=f"{target.name}'s Infractions", description=f'{target.name} currently has {moderation.get_infractions(target.id)} infractions.')
        await ctx.send(embed=embed)
        
    def __init__(self, client):
        self.client : commands.Bot = client
def setup(client):
    client.add_cog(infractions(client))    