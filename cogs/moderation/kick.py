from discord.ext import commands
from utilities.db_handler import infractions as infr
from utilities import moderation_handler as moderation
import discord
import time

class kick(commands.Cog):
    @commands.command()
    @commands.has_permissions(kick_members=True)
    async def kick(self, ctx, user, *, kick_reason):
        if len(ctx.message.mentions) > 1 or len(ctx.message.mentions) < 1: await ctx.send('Invalid arguments (not enough or too many mentions)'); return
        target = ctx.guild.get_member(ctx.message.mentions[0].id)

        await moderation.kick_user(ctx, target, reason=kick_reason)
        await ctx.send(f'Successfully kicked <@{target.id}>.')

    def __init__(self, client):
        self.client : commands.Bot = client
def setup(client):
    client.add_cog(kick(client))