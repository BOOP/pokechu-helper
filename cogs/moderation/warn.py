from discord.ext import commands
from utilities.db_handler import infractions as infr
from utilities.db_handler import db
from utilities import moderation_handler as moderation
import discord

class warn(commands.Cog):
    @commands.command(aliases=['warn'])
    @commands.has_any_role('Staff', 'Admin')
    async def warning(self, ctx, user, *, warn_reason):
        if len(ctx.message.mentions) > 1 or len(ctx.message.mentions) < 1: await ctx.send('Invalid arguments (not enough or too many mentions)'); return
        target = ctx.guild.get_member(ctx.message.mentions[0].id)

        await moderation.warn_user(ctx, target, reason=warn_reason)
        await ctx.send(f'Successfully warned <@{target.id}>.' + '\n' + f'**Reason**: {warn_reason}')
    
    @commands.command(aliases=['rwarn', 'rinfr', 'rinfraction'])
    @commands.has_any_role('Staff', 'Admin')
    async def rwarning(self, ctx, infr_id):
        try: infr_id = int(infr_id)
        except: await ctx.send('Invalid infraction ID. Please try again.'); return
        if not moderation.check_infr_id(infr_id): await ctx.send('Sorry, that infraction ID was not found in our database.'); return

        db.execute(infractions.delete().where(infractions.c.infraction_id==infr_id))
        await ctx.send('The infraction has been removed.')

    def __init__(self, client):
        self.client : commands.Bot = client
def setup(client):
    client.add_cog(warn(client))    