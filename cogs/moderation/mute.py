from discord.ext import commands
from utilities.db_handler import infractions as infr
from utilities.logging_types import Loggers, LoggingType
from utilities import moderation_handler as moderation
import discord

class mute(commands.Cog):
    @commands.command()
    @commands.bot_has_permissions(manage_roles=True)
    @commands.has_any_role('Staff', 'Admin')
    async def mute(self, ctx, user, *, mute_reason):
        if len(ctx.message.mentions) > 1 or len(ctx.message.mentions) < 1: await ctx.send('Invalid arguments (not enough or too many mentions)'); return
        target = ctx.guild.get_member(ctx.message.mentions[0].id)

        await moderation.mute_user(target, ctx, reason=mute_reason)
        await ctx.send(f'Successfully muted <@{target.id}>')
        
    @commands.command()
    @commands.bot_has_permissions(manage_roles=True)
    @commands.has_any_role('Staff', 'Admin')
    async def unmute(self, ctx, user, *, unmute_reason="None Provided"):
        if len(ctx.message.mentions) > 1 or len(ctx.message.mentions) < 1: await ctx.send('Invalid arguments (not enough or too many mentions)'); return
        target = ctx.guild.get_member(ctx.message.mentions[0].id)
        muted_role = discord.utils.get(ctx.guild.roles, name="Muted")
        lt = LoggingType(Loggers.UNMUTE, 'unmuted', 0xe2e200)

        await target.remove_roles(muted_role, reason=unmute_reason)
        await moderation.log_action(lt, ctx, target.id, reason=unmute_reason)
        await ctx.send(f'Succesfully unmuted <@{target.id}>.')

    def __init__(self, client):
        self.client : commands.Bot = client
def setup(client):
    client.add_cog(mute(client))