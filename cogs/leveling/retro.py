from discord.ext import commands
from utilities import levels_handler as lvl
from utilities.db_handler import db, xp
import discord
from utilities.level_roles import LevelRoles
active = False
'''
INACTIVE COMMAND

This command is not currently active.
Purpose: Retroactively apply level roles to users who already achieved a goal.

'''

async def active_state(ctx): return active

class retro(commands.Cog):
    @commands.command()
    @commands.check(active_state)
    async def retro(self, ctx):
        print('Searching...')
        res = db.execute(f'SELECT user_id,xp,level FROM xp WHERE level > 10 ORDER BY level DESC')
        for user in res.fetchall():
            target = ctx.guild.get_member(int(user[0]))
            xp = user[1]
            level = int(str(user[2])[:-1] + '0') # must round down to work with the way LevelRoles was designed            

            print(f'{target.name} is level {level} with {xp} XP')
            if 10 <= level <= 19:
                print('Applying Boulder Badge...')
                role_name = str(LevelRoles(level)).split('.')[1].replace('_', ' ')
                level_role = discord.utils.get(ctx.guild.roles, name=role_name)

                await target.add_roles(level_role, reason='Retroactive Application of Level Role')
                print('Applied Boulder Badge to ' + target.name)
            elif 20 <= level <= 29:
                print('Applying Cascade Badge...')
                role_name = str(LevelRoles(level)).split('.')[1].replace('_', ' ')
                level_role = discord.utils.get(ctx.guild.roles, name=role_name)

                await target.add_roles(level_role, reason='Retroactive Application of Level Role')
                print('Applied Cascade Badge to ' + target.name)

    def __init__(self, client):
        self.client = client
def setup(client):
    client.add_cog(retro(client))