from discord.ext import commands
from utilities import levels_handler as lvl
from utilities import db_handler
from utilities.level_roles import LevelRoles
import discord
import time

class levels(commands.Cog):
    @commands.Cog.listener()
    async def on_message(self, ctx : commands.Context):
        author_id = ctx.author.id
        if ctx.author.bot or ctx.content.startswith(('~', '!', ',', '?', '+', '-', '.', 'pls', 'OwO')) or ctx.channel.id == 717536381030629429: return
        if not lvl.check_user_id(author_id):
            db_handler.db.execute(db_handler.xp.insert(), [{
                "user_id": ctx.author.id,
                "xp_block_until": time.time() + 30,
                "messages": 1,
                "level": 1,
                "xp": lvl.generate_xp(ctx.content)
            }])

        if lvl.get_user_block_time(author_id) > time.time(): return
        
        new_xp = lvl.get_user_xp(author_id) + lvl.generate_xp(ctx.content)
        lvl.add_user_messages(author_id, 1)
        lvl.set_user_xp(ctx.author.id, new_xp)
        lvl.set_user_block_time(author_id, time.time() + 30)

        if new_xp < 0: return
        current_level = lvl.get_user_level(author_id)
        xp_level = lvl.calculate_level(new_xp)
        if xp_level > current_level: 
            lvl.add_user_levels(ctx.author.id, 1)
            if xp_level in [10, 20, 30, 40, 50, 60, 70, 80]:
                role_name = str(LevelRoles(xp_level)).split('.')[1].replace('_', ' ')
                level_role = discord.utils.get(ctx.guild.roles, name=role_name)

                await ctx.author.add_roles(level_role, reason=f'Level Role (Achieved Level {xp_level})')

            await ctx.channel.send(f'Good job, <@{author_id}>! You are now level {lvl.calculate_level(new_xp)}.', delete_after=10.0)
            
    @commands.Cog.listener()
    async def on_message_delete(self, ctx : commands.Context):
        if not lvl.check_user_id(ctx.author.id): return
        current_xp = lvl.get_user_xp(ctx.author.id)
        amount_to_remove = lvl.generate_xp(ctx.content)
        if current_xp > amount_to_remove: lvl.set_user_xp(ctx.author.id, current_xp - amount_to_remove)

    def __init__(self, client):
        self.client = client
def setup(client):
    client.add_cog(levels(client))