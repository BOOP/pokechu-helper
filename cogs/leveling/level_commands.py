from discord.ext import commands
from utilities import levels_handler as lvl
from utilities.db_handler import db, xp
import discord
import time

class level_commands(commands.Cog):
    @commands.command(aliases=['rank'])
    async def level(self, ctx):
        if not lvl.check_user_id(ctx.author.id): await ctx.send('Sorry, you haven\'t gained any XP yet.'); return
        await ctx.send(f'''You are currently level {lvl.get_user_level(ctx.author.id)} ({lvl.get_user_xp(ctx.author.id)} XP) with {lvl.get_user_messages(ctx.author.id)} messages!''')

    @commands.command(aliases=['lb', 'levels'])
    async def leaderboard(self, ctx, leaderboard_length=5):
        res = db.execute(f'SELECT user_id,xp,level FROM xp ORDER BY xp DESC LIMIT {leaderboard_length}')
        
        embed=discord.Embed(title="**Leveling Leaderboard**", color=0x12a0ed)
        embed.set_footer(text=f'Super High Quality Bot Made By BOOP')
        position = 1
        for user in res.fetchall():
            if position == 1: embed.add_field(name=f':first_place: **Position {position}**', value=f'**User:** <@{user[0]}>\n**Level:** {user[2]}\n**XP:** {user[1]}', inline=False)
            elif position == 2: embed.add_field(name=f':second_place: **Position {position}**', value=f'**User:** <@{user[0]}>\n**Level:** {user[2]}\n**XP:** {user[1]}', inline=False)
            elif position == 3: embed.add_field(name=f':third_place: **Position {position}**', value=f'**User:** <@{user[0]}>\n**Level:** {user[2]}\n**XP:** {user[1]}', inline=False)
            elif position == 4 or position == 5: embed.add_field(name=f':medal: **Position {position}**', value=f'**User:** <@{user[0]}>\n**Level:** {user[2]}\n**XP:** {user[1]}', inline=False)
            else: embed.add_field(name=f'**Position {position}**', value=f'**User:** <@{user[0]}>\n**Level:** {user[2]}\n**XP:** {user[1]}', inline=False)

            position += 1

        await ctx.send(embed=embed)


    # Begin XP administratioan commands
    @commands.group(aliases=['xp'])
    @commands.has_role("Admin")
    async def admin(self, ctx):
        if not ctx.subcommand_passed:
            xp_commands_description = '`~xp set <user> <xp>` - Sets a user\'s XP\n`~xp add <user> <xp>` -  Adds to a user\'s XP.\n`~xp remove <user> <xp>` - Removes from a user\'s XP.'
            embed = discord.Embed(title='XP Administration Commands', description='Commands available to administrators that allow control of XP.\n' + xp_commands_description)

            await ctx.send(embed=embed)

    @admin.command()
    @commands.has_role("Admin")
    async def set(self, ctx, user, new_value):
        try: new_value = int(new_value)
        except: await ctx.send('Invalid XP value!'); return
        
        if len(ctx.message.mentions) > 1 or len(ctx.message.mentions) < 1: await ctx.send('Invalid arguments (not enough or too many mentions)'); return
        target = ctx.guild.get_member(ctx.message.mentions[0].id)
        
        if not lvl.check_user_id(target.id): await ctx.send('That user has not been initated in the database. They must send a message first.'); return

        lvl.set_user_xp(target.id, new_value)
        lvl.set_user_level(target.id, lvl.calculate_level(new_value))

        await ctx.send(f'Changed {target.name}\'s XP to {new_value} (additionally, their level is {lvl.calculate_level(new_value)}).')

    @admin.command()
    @commands.has_role("Admin")
    async def add(self, ctx, user, new_value):
        try: new_value = int(new_value)
        except: await ctx.send('Invalid XP value!'); return
        
        if len(ctx.message.mentions) > 1 or len(ctx.message.mentions) < 1: await ctx.send('Invalid arguments (not enough or too many mentions)'); return
        target = ctx.guild.get_member(ctx.message.mentions[0].id)

        if not lvl.check_user_id(target.id): await ctx.send('That user has not been initated in the database. They must send a message first.'); return
        
        old_xp = lvl.get_user_xp(target.id)
        new_xp = old_xp + new_value

        if lvl.calculate_level(new_xp) > lvl.calculate_level(old_xp):
            lvl.set_user_xp(target.id, new_xp)
            lvl.set_user_level(target.id, lvl.calculate_level(new_xp))

            await ctx.send(f'Changed {target.name}\'s XP to {new_xp} (additionally, their level is {lvl.calculate_level(new_xp)}).')
        else:
            lvl.set_user_xp(new_xp)

            await ctx.send(f'Changed {target.name}\'s XP to {new_xp} (additionally, their level has not been altered).')

    @admin.command(aliases=['subtract'])
    @commands.has_role("Admin")
    async def remove(self, ctx, user, new_value):
        try: new_value = int(new_value)
        except: await ctx.send('Invalid XP value!'); return
        
        if len(ctx.message.mentions) > 1 or len(ctx.message.mentions) < 1: await ctx.send('Invalid arguments (not enough or too many mentions)'); return
        target = ctx.guild.get_member(ctx.message.mentions[0].id)

        if not lvl.check_user_id(target.id): await ctx.send('That user has not been initated in the database. They must send a message first.'); return
        
        old_xp = lvl.get_user_xp(target.id)
        new_xp = old_xp - new_value

        if lvl.calculate_level(new_xp) < lvl.calculate_level(old_xp):
            lvl.set_user_xp(target.id, new_xp)
            lvl.set_user_level(target.id, lvl.calculate_level(new_xp))

            await ctx.send(f'Changed {target.name}\'s XP to {new_xp} (additionally, their level is {lvl.calculate_level(new_xp)}).')
        else:
            lvl.set_user_xp(new_xp)

            await ctx.send(f'Changed {target.name}\'s XP to {new_xp} (additionally, their level has not been altered).')


    def __init__(self, client):
        self.client = client
def setup(client):
    client.add_cog(level_commands(client))