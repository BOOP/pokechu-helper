from discord.ext import commands
from utilities import *
import discord


class manage_suggestions(commands.Cog):
    @commands.command(aliases=['accept'])
    @commands.has_any_role("Admin", "Staff")
    async def accept_suggestion(self, ctx : commands.Context, suggestion_or_message_id):
        try: suggestion_or_message_id = int(suggestion_or_message_id)
        except: await ctx.send('Invalid suggestion ID')

        if not suggestions_handler.check_suggestion_or_message_id(suggestion_or_message_id): await ctx.send('Unable to find suggestion.'); return
        if suggestion_or_message_id > 999999:
            message = await suggestions_handler.fetch_suggestion(suggestion_or_message_id, ctx)
            suggestion_id = suggestions_handler.get_suggestion_id(suggestion_or_message_id)
            accepted_channel = discord.utils.get(ctx.guild.text_channels, name='accepted-suggestions')

            embed = discord.Embed(title='Suggestion Accepted', description=message.embeds[0].description, color=0x00a521)
            embed.set_author(name=message.embeds[0].author.name, icon_url=message.embeds[0].author.icon_url)
            embed.set_footer(text=f'Suggestion ID: {suggestion_id} | Made By BOOP')

            await message.delete()
            accepted_embed = await accepted_channel.send(embed=embed)

            suggestions_handler.set_message_id(suggestion_id, accepted_embed.id)
            suggestions_handler.set_suggestion_state(suggestion_id, 1)

            await ctx.send(f'The suggestion (`{suggestion_id}`) has been accepted.')

        else:
            message = await suggestions_handler.fetch_suggestion(suggestion_or_message_id, ctx)
            accepted_channel = discord.utils.get(ctx.guild.text_channels, name='accepted-suggestions')

            embed = discord.Embed(title='Suggestion Accepted', description=message.embeds[0].description, color=0x00a521)
            embed.set_author(name=message.embeds[0].author.name, icon_url=message.embeds[0].author.icon_url)
            embed.set_footer(text=f'Suggestion ID: {suggestion_or_message_id} | Made By BOOP')

            await message.delete()
            accepted_embed = await accepted_channel.send(embed=embed)

            suggestions_handler.set_message_id(suggestion_or_message_id, accepted_embed.id)
            suggestions_handler.set_suggestion_state(suggestion_or_message_id, 1)

            await ctx.send(f'The suggestion (`{suggestion_or_message_id}`) has been accepted.')

    @commands.command(aliases=['deny'])
    @commands.has_any_role("Admin", "Staff")
    async def deny_suggestion(self, ctx : commands.Context, suggestion_or_message_id):
        try: suggestion_or_message_id = int(suggestion_or_message_id)
        except: await ctx.send('Invalid suggestion ID')

        if not suggestions_handler.check_suggestion_or_message_id(suggestion_or_message_id): await ctx.send('Unable to find suggestion.'); return
        if suggestion_or_message_id > 999999:
            message = await suggestions_handler.fetch_suggestion(suggestion_or_message_id, ctx)
            suggestion_id = suggestions_handler.get_suggestion_id(suggestion_or_message_id)
            denied_channel = discord.utils.get(ctx.guild.text_channels, name='denied-suggestions')

            embed = discord.Embed(title='Suggestion Denied', description=message.embeds[0].description, color=0xfc1e21)
            embed.set_author(name=message.embeds[0].author.name, icon_url=message.embeds[0].author.icon_url)
            embed.set_footer(text=f'Suggestion ID: {suggestion_id} | Made By BOOP')

            await message.delete()
            denied_embed = await denied_channel.send(embed=embed)

            suggestions_handler.set_message_id(suggestion_id, denied_embed.id)
            suggestions_handler.set_suggestion_state(suggestion_id, 2)

            await ctx.send(f'The suggestion (`{suggestion_id}`) has been denied.')

        else:
            message = await suggestions_handler.fetch_suggestion(suggestion_or_message_id, ctx)
            denied_channel = discord.utils.get(ctx.guild.text_channels, name='denied-suggestions')

            embed = discord.Embed(title='Suggestion Denied', description=message.embeds[0].description, color=0xfc1e21)
            embed.set_author(name=message.embeds[0].author.name, icon_url=message.embeds[0].author.icon_url)
            embed.set_footer(text=f'Suggestion ID: {suggestion_or_message_id} | Made By BOOP')

            await message.delete()
            denied_embed = await denied_channel.send(embed=embed)

            suggestions_handler.set_message_id(suggestion_or_message_id, denied_embed.id)
            suggestions_handler.set_suggestion_state(suggestion_or_message_id, 2)

            await ctx.send(f'The suggestion (`{suggestion_or_message_id}`) has been denied.')

    @commands.command(aliases=['delete'])
    @commands.has_role("Admin")
    async def delete_suggestion(self, ctx : commands.Context, suggestion_or_message_id):
        try: suggestion_or_message_id = int(suggestion_or_message_id)
        except: await ctx.send('Invalid suggestion ID')

        if not suggestions_handler.check_suggestion_or_message_id(suggestion_or_message_id): await ctx.send('Unable to find suggestion.'); return
        await ctx.message.delete()
        if suggestion_or_message_id > 999999:
            message = await suggestions_handler.fetch_suggestion(suggestion_or_message_id, ctx)
            suggestion_id = suggestions_handler.get_suggestion_id(suggestion_or_message_id)

            query = db_handler.suggestions.delete().where(db_handler.suggestions.c.message_id==suggestion_or_message_id)
            db_handler.db.execute(query)

            await message.delete()
            await ctx.send(f'The suggestion (`{suggestion_id}`) has been deleted.', delete_after=5.0)
        else:
            message = await suggestions_handler.fetch_suggestion(suggestion_or_message_id, ctx)
            
            query = db_handler.suggestions.delete().where(db_handler.suggestions.c.suggestion_id==suggestion_or_message_id)
            db_handler.db.execute(query)

            await message.delete()
            await ctx.send(f'The suggestion (`{suggestion_or_message_id}`) has been deleted.', delete_after=5.0)

    def __init__(self, client):
        self.client = client
def setup(client):
    client.add_cog(manage_suggestions(client))