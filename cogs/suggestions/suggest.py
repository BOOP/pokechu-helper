from discord.ext import commands
from utilities import *
import discord

class suggestion(commands.Cog):
    @commands.command()
    async def suggest(self, ctx, *, suggestion):
        channel = discord.utils.get(ctx.guild.text_channels, name='suggestions')
        suggestion_id = suggestions_handler.gen_suggestion_id(6)

        embed = discord.Embed(title='New Suggestion', description=suggestion, color=0x0165c1)
        embed.set_author(name=ctx.author.name, icon_url=ctx.author.avatar_url)
        embed.set_footer(text=f'Suggestion ID: {suggestion_id} | Made By BOOP')

        suggestion_embed = await channel.send(embed=embed)
        suggestions_handler.create_suggestion(suggestion_id, suggestion_embed.id, ctx.author.id, 0)

        embed = discord.Embed(title='Created Successfully', description=f'Your suggestion (#{suggestion_id}) has been created successfully!' + "\n" + f'You can view it in <#{channel.id}>', color=0x01c13b)
        embed.set_footer(text=f'Made By BOOP')
        await ctx.send(embed=embed)

    def __init__(self, client):
        self.client = client
def setup(client):
    client.add_cog(suggestion(client))  