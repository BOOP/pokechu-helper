from random import choice
from discord.ext import commands
import discord

class spoon_misc(commands.Cog):
    @commands.command()
    async def spoon(self, ctx):
        spoons = ['https://dijf55il5e0d1.cloudfront.net/images/na/2/6/9/26923_1000.jpg', 
                'https://candid-images.s3-us-west-2.amazonaws.com/271820335819.jpg', 
                'https://cdn.cutleryandmore.com/media/images/10143.jpg', 
                'http://i.huffpost.com/gen/1302381/images/o-SPOON-facebook.jpg']

        embed = discord.Embed(title='Sp00n')
        embed.set_image(url=choice(spoons))

        await ctx.send(embed=embed)
    def __init__(self, client):
        self.client = client
def setup(client):
    client.add_cog(spoon_misc(client))