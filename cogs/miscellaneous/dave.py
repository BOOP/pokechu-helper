from discord.ext import commands
from utilities.db_handler import dave_exemptions, db
import discord
import uuid

def check_exempt(user_id):
    query = dave_exemptions.select().where(dave_exemptions.c.user_id==user_id)
    res = db.execute(query)
    return True if res.rowcount > 0 else False

class daves_misc(commands.Cog):
    @commands.Cog.listener()
    async def on_message(self, ctx):
        if ctx.author.id == 721582760413757453: return

        if not check_exempt(ctx.author.id):
            if ctx.mentions:
                for m in ctx.mentions:
                    if m.id == 439477702496485377:
                        await ctx.delete()
                        await ctx.channel.send(f'<@{ctx.author.id}>, do not mention the almighty Dave!')

        if ctx.author.id == 439477702496485377:
            if "see" in ctx.content.lower():
                await ctx.channel.send("i saw")

    @commands.Cog.listener()
    async def on_message_edit(self, ctx1, ctx2):
        if ctx2.mentions:
            for m in ctx2.mentions:
                if m.id == 439477702496485377:
                    await ctx2.delete()
                    await ctx2.channel.send(f'<@{ctx2.author.id}>, do not mention the almighty Dave!')

    @commands.command()
    async def dave(self, ctx):
        await ctx.send("You have been pinged, <@439477702496485377>!")

    def __init__(self, client):
        self.client = client
def setup(client):
    client.add_cog(daves_misc(client))