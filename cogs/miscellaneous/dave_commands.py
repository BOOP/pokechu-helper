from discord.ext import commands
from utilities.db_handler import dave_exemptions, db
import discord

async def dave_check(ctx):
    return ctx.author.id in [439477702496485377, 720098157282721872]

def check_exempt(user_id):
    res = db.execute(dave_exemptions.select().where(dave_exemptions.c.user_id==user_id))
    return True if res.rowcount > 0 else False

class dave_commands(commands.Cog):
    @commands.command()
    @commands.check(dave_check)
    async def allow(self, ctx, user, reason=None):
        if len(ctx.message.mentions) > 1 or len(ctx.message.mentions) < 1: await ctx.send('Invalid arguments (not enough or too many mentions)'); return
        target = ctx.guild.get_member(ctx.message.mentions[0].id)

        query = dave_exemptions.insert().values([{
            "user_id": target.id,
            "reason": reason
        }])

        db.execute(query)
        await ctx.send(f'<@{target.id}> can now mention you.')

    @commands.command()
    @commands.check(dave_check)
    async def remove(self, ctx, user, reason=None):
        if len(ctx.message.mentions) > 1 or len(ctx.message.mentions) < 1: await ctx.send('Invalid arguments (not enough or too many mentions)'); return
        target = ctx.guild.get_member(ctx.message.mentions[0].id)

        if not check_exempt(target.id): await ctx.send('User was never allowed.'); return
        db.execute(dave_exemptions.delete().where(dave_exemptions.c.user_id==target.id))

        await ctx.send(f'<@{target.id}> is no longer allowed to mention you.')
    def __init__(self, client):
        self.client = client
def setup(client):
    client.add_cog(dave_commands(client))