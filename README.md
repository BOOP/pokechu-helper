# Pokechu/BOOP's Helper

This project was originally designed to be used on the Pokechu Discord bot official support server. Any use outside of this is not guaranteed to work, and is not recommended. Many values are hard-coded for ease-of-development, so changing features to work in alternate servers may provide where results. This repository 
does not track any previous versions, and instead is beginning at the v4 development stage. This is a full rewrite following v3 to allow easier expandability,
and provide easier deployment (ideally using Docker containers and DeployHQ or alternative).

## TODO

1. [X] Utils
    * [X] Database Handler
    * [X] Suggestion Handler
    * [X] Level Handler
2. [X] Suggestions
    * [X] Create suggestion
    * [X] Accept suggestions of any state
    * [X] Deny suggestions of any state
    * [X] Delete suggestions of any state   
    * [ ] (Optional) Allow suggestion author to delete their own suggestion
    * [ ] (Optional) PM suggestion author when their suggestion is accepted
2. [X] Leveling
    * [X] Award XP
    * [X] Calculate leveling
    * [X] XP command
    * [X] Leaderboard commmand
    * [X] (Optional) Subtract XP when a message is deleted
    * [X] (Optional) XP administration commands (set, add, subtract)
    * [X] (Optional) Level roles
3. [X] Moderation
    * [X] Kick command
    * [X] Ban command
    * [X] Mute command (optionally, a temp mute command)
    * [X] Purge
    * [X] Infractions/warning command
    * [X] Logging (deleted/edited messages)
    * [X] Infractions command
    * [ ] (Optional) Auto warning actions
4. [X] Miscellaneous
    * [X] Dave tag block & command (allow dave to add/remove exemptions)
    * [X] Axel ily command ("We love you too, (ping axel)!")
    * [X] Spoon command (sends picture of spoon)
5. [X] Per-Command Error Handling (and other improvements)
    * [X] No Permission Handler
    * [X] Not enough arguments handler
    * [X] Improve Dockerfile
6. [ ] Minecraft Link

