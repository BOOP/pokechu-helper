from random import randint
from sqlalchemy import Table
from utilities import db_handler
import discord.ext.commands
import discord.utils
db = db_handler.db
suggestions : Table = db_handler.suggestions

'''
return: suggestion id, message id, author id, state
state:
0 - open
1 - accepted
2 - closed

✓ Generate suggestion ID
✓ Create suggestion
✓ Check suggestion
✓ Get suggestion (message id or suggestion id)
✓ Get suggestion ID (message ID)
✓ Get message ID (suggestion ID)
✓ Get suggestion author (message id or suggestion id)
✓ Get suggestion state
✓ Set message ID
✓ Set suggestion state

'''

class InvalidIDError(Exception):
    pass # This will be raised if an invalid ID is given, or we're unable to find a suggestion with the given ID.

def check_suggestion_or_message_id(suggestion_or_message_id : int) -> bool:
    """
    Checks the given suggestion ID or message ID against the database to check whether it exists.
    The result of this operation is returned in the form of a bool.
    """
    if suggestion_or_message_id > 999999:
        query = suggestions.select().where(suggestions.c.message_id==suggestion_or_message_id)
        res = db.execute(query)
        if res.rowcount > 0: return True
        else: return False
    else:
        query = suggestions.select().where(suggestions.c.suggestion_id==suggestion_or_message_id)
        res = db.execute(query)
        if res.rowcount > 0: return True
        else: return False

def gen_suggestion_id(id_length : int) -> int:
    """
    Generates a suggestion ID that is within the given length.
    Returns the suggestion ID as an int.
    """
    min_num = 10**(id_length -1)
    max_num = (10**id_length)-1
    suggestion_id = randint(min_num, max_num)

    while check_suggestion_or_message_id(suggestion_id):
        suggestion_id = randint(min_num, max_num)

    return suggestion_id

def create_suggestion(suggestion_id : int, message_id : str or int, author_id : str or int, state : int):
    message_id = str(message_id)
    author_id = str(author_id)

    db.execute(suggestions.insert(), [
        {"suggestion_id": suggestion_id,
        "message_id": message_id,
        "author_id": author_id,
        "state": state}
    ])

def get_suggestion_id(message_id : int or str) -> int:
    message_id = int(message_id)
    if not check_suggestion_or_message_id(message_id): raise InvalidIDError("Unable to find the given ID")

    res = db.execute(suggestions.select().where(suggestions.c.message_id==message_id)).fetchone()
    return res[0]

def get_message_id(suggestion_id : int or str) -> int:
    suggestion_id = int(suggestion_id)
    if not check_suggestion_or_message_id(suggestion_id): raise InvalidIDError("Unable to find the given ID")

    res = db.execute(suggestions.select().where(suggestions.c.suggestion_id==suggestion_id)).fetchone()
    return res[1]

def get_suggestion_author(suggestion_or_message_id : int or str) -> int:
    print(suggestion_or_message_id)
    #suggestion_or_message_id = int(check_suggestion_or_message_id)
    if not check_suggestion_or_message_id(suggestion_or_message_id): raise InvalidIDError("Unable to find the given ID")

    if suggestion_or_message_id > 999999:
        res = db.execute(suggestions.select().where(suggestions.c.message_id==suggestion_or_message_id)).fetchone()
        return res[2]
    else:
        res = db.execute(suggestions.select().where(suggestions.c.suggestion_id==suggestion_or_message_id)).fetchone()
        return res[2]

def get_suggestion_state(suggestion_or_message_id : int or str) -> int:
    suggestion_or_message_id = int(suggestion_or_message_id)
    if not check_suggestion_or_message_id(suggestion_or_message_id): raise InvalidIDError("Unable to find the given ID")

    if suggestion_or_message_id > 999999:
        res = db.execute(suggestions.select().where(suggestions.c.message_id==suggestion_or_message_id)).fetchone()
        return res[3]
    else:
        res = db.execute(suggestions.select().where(suggestions.c.suggestion_id==suggestion_or_message_id)).fetchone()
        return res[3]

async def fetch_suggestion(suggestion_or_message_id : int or str, ctx : discord.ext.commands.Context) -> discord.Message:
    suggestion_or_message_id = int(suggestion_or_message_id)
    if not check_suggestion_or_message_id(suggestion_or_message_id): raise InvalidIDError("Unable to find the given ID") 
    
    # channel = discord.utils.get(ctx.guild.text_channels, name='suggestions')
    # accepted_channel = discord.utils.get(ctx.guild.text_channels, name='accepted-suggestions')
    # denied_channel = discord.utils.get(ctx.guild.text_channels, name='denied-suggestions')

    state = get_suggestion_state(suggestion_or_message_id)

    if suggestion_or_message_id > 999999:
        if state == 0:
            channel = discord.utils.get(ctx.guild.text_channels, name='suggestions')
            fetchedMessage = await channel.fetch_message(suggestion_or_message_id)

            return fetchedMessage
        elif state == 1:
            accepted_channel = discord.utils.get(ctx.guild.text_channels, name='accepted-suggestions')
            fetchedMessage = await accepted_channel.fetch_message(suggestion_or_message_id)

            return fetchedMessage
        elif state == 2:
            denied_channel = discord.utils.get(ctx.guild.text_channels, name='denied-suggestions')
            fetchedMessage = await denied_channel.fetch_message(suggestion_or_message_id)

            return fetchedMessage
    else:
        if state == 0:
            channel = discord.utils.get(ctx.guild.text_channels, name='suggestions')
            fetchedMessage = await channel.fetch_message(get_message_id(suggestion_or_message_id))

            return fetchedMessage
        elif state == 1:
            accepted_channel = discord.utils.get(ctx.guild.text_channels, name='accepted-suggestions')
            fetchedMessage = await accepted_channel.fetch_message(get_message_id(suggestion_or_message_id))

            return fetchedMessage
        elif state == 2:
            denied_channel = discord.utils.get(ctx.guild.text_channels, name='denied-suggestions')
            fetchedMessage = await denied_channel.fetch_message(get_message_id(suggestion_or_message_id))

            return fetchedMessage

def set_suggestion_state(suggestion_or_message_id : int or str, new_state : int):
    suggestion_or_message_id = int(suggestion_or_message_id)
    if not check_suggestion_or_message_id(suggestion_or_message_id): raise InvalidIDError("Unable to find the given ID.")

    if suggestion_or_message_id > 999999:
        query = suggestions.update().where(suggestions.c.message_id==suggestion_or_message_id).values(state=new_state)
        db.execute(query)
    else:
        query = suggestions.update().where(suggestions.c.suggestion_id==suggestion_or_message_id).values(state=new_state)
        db.execute(query)

def set_message_id(suggestion_or_message_id : int or str, new_message_id : int or str):
    suggestion_or_message_id = int(suggestion_or_message_id)
    if not check_suggestion_or_message_id(suggestion_or_message_id): raise InvalidIDError("Unable to find the given ID.")

    if suggestion_or_message_id > 999999:
        query = suggestions.update().where(suggestions.c.message_id==suggestion_or_message_id).values(message_id=new_message_id)
        db.execute(query)
    else:
        query = suggestions.update().where(suggestions.c.suggestion_id==suggestion_or_message_id).values(message_id=new_message_id)
        db.execute(query)
