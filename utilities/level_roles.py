from enum import Enum

class LevelRoles(Enum):
    Boulder_Badge = 10
    Cascade_Badge = 20
    Thunder_Badge = 30
    Rainbow_Badge = 40
    Soul_Badge = 50
    Marsh_Badge = 60
    Volcano_Badge = 70
    Earth_Badge =  80