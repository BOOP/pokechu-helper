from enum import Enum
from discord import Color

class Loggers(Enum):
    WARNING = 100
    REMOVE_WARNING = 105 # action
    MUTE =  110
    REMOVE_MUTE = 115 # action
    KICK = 120
    BAN = 130
    REMOVE_BAN = 135 # action
    PURGE = 140 # action

class LoggingType:
    def __init__(self, logger_type : Loggers, friendly_name : str, embed_color : Color or int):
        self.type = logger_type
        self.code = logger_type.value
        self.name = friendly_name
        self.color = embed_color