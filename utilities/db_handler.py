from sqlalchemy import create_engine, MetaData, Table, Column, CHAR, VARCHAR, INT, BIGINT, exists
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())
from os import environ

eng = create_engine(f"mysql+pymysql://{environ.get('SQL_DB_USER')}:{environ.get('SQL_DB_PASS')}@{environ.get('SQL_DB_HOST')}/{environ.get('SQL_DB_NAME')}", echo=False, pool_size=10)
db = eng.connect()
meta = MetaData()

suggestions = Table(
            'suggestions', meta,
            Column('suggestion_id', INT(), primary_key=True, autoincrement=False),
            Column('message_id', CHAR(length=18), unique=True),
            Column('author_id', CHAR(length=18)),
            Column('state', INT())
)
xp = Table(
            'xp', meta,
            Column('user_id', CHAR(length=18), primary_key=True, autoincrement=False),
            Column('xp_block_until', BIGINT()),
            Column('messages', INT()),
            Column('level', INT()),
            Column('xp', INT())
)

actions = Table(
            'actions', meta,
            Column('action_id', INT(), primary_key=True, autoincrement=False),
            Column('executor_id', CHAR(length=18), nullable=False),
            Column('target_id', CHAR(length=18), nullable=True),
            Column('type', INT(), nullable=False),
            Column('reason', VARCHAR(length=255), nullable=True)
)

infractions = Table(
            'infractions', meta,
            Column('infraction_id', INT(), primary_key=True, autoincrement=False),
            Column('executor_id', CHAR(length=18), nullable=False),
            Column('target_id', CHAR(length=18), nullable=False),
            Column('type', INT(), nullable=False),
            Column('reason', VARCHAR(length=255), nullable=False)
)

dave_exemptions = Table(
            'dave', meta,
            Column('user_id', CHAR(length=18), unique=True, nullable=False),
            Column('reason', VARCHAR(length=255), unique=False, nullable=True)
)

meta.create_all(eng)