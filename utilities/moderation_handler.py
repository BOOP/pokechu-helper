from random import randint
from sqlalchemy import Table
from utilities import db_handler
from utilities.logging_types import Loggers, LoggingType
from discord.ext import commands
from datetime import datetime
import discord.utils
import enum
db = db_handler.db
infr:Table = db_handler.infractions
acti:Table = db_handler.actions

'''
types:
100 - Warning
105 - Remove warning
110 - Mute
115 - Unmute
120 - Kick
130 - Ban
135 - Unban

200 - Edited Message
210 - Deleted Message

TODO: Basic Functions

* Kick Function
* Ban Function
* Log function
* Infraction Functions (get, add, etc)
* Logging
'''

def get_infractions(user_id) -> int:
    if check_user_id(user_id):
        res = db.execute(f'SELECT infraction_id,type FROM infractions WHERE infractions.target_id={user_id}')
        return res.rowcount
    else: return 0

def check_acti_id(acti_id_to_check) -> bool:
    query = acti.select().where(acti.c.action_id==acti_id_to_check)
    res = db.execute(query)
    if res.rowcount > 0: return True
    else: return False

def check_infr_id(infr_id_to_check) -> bool:
    query = infr.select().where(infr.c.infraction_id==infr_id_to_check)
    res = db.execute(query)
    if res.rowcount > 0: return True
    else: return False

def check_user_id(user_id_to_check) -> bool:
    query = infr.select().where(infr.c.target_id==user_id_to_check)
    res = db.execute(query)
    if res.rowcount > 0: return True
    else: return False

def gen_id(id_length : int, id_type) -> int:
    min_num = 10**(id_length -1)
    max_num = (10**id_length)-1
    obj_id = randint(min_num, max_num)

    if id_type == 'action': 
        while check_acti_id(obj_id): obj_id = randint(min_num, max_num)
    elif id_type == 'infraction': 
        while check_infr_id(obj_id): obj_id = randint(min_num, max_num)

    return int(obj_id)

async def log_infraction(action:LoggingType, ctx:commands.Context, target:discord.User, reason:str):
    logging_channel:discord.TextChannel = discord.utils.get(ctx.guild.channels, name='logging') or discord.utils.get(ctx.guild.channels, name='action-log') or discord.utils.get(ctx.guild.channels, name='action-logging')
    embed = discord.Embed(title='Moderation Action', description=f'<@{target.id}> was {action.name} by <@{ctx.author.id}>.', color=action.color)
    infr_id = gen_id(8, 'infraction')

    query = infr.insert().values([{
        "infraction_id": infr_id,
        "executor_id": ctx.author.id,
        "target_id": target.id,
        "type": action.code,
        "reason": reason
    }])

    embed.add_field(name='Time (UTC)', value=datetime.utcnow().strftime("%b %d %Y, %I:%M:%S %p"))
    embed.add_field(name='Reason', value=reason)
    embed.set_footer(text='Infraction ID: ' + str(infr_id))
    db.execute(query)
    await logging_channel.send(embed=embed)

async def log_action(action:LoggingType, ctx:commands.Context, target_id:int=None, reason:str=None, **kwargs):
    if not reason: reason = "None Provided"
    logging_channel:discord.TextChannel = discord.utils.get(ctx.guild.channels, name='logging') or discord.utils.get(ctx.guild.channels, name='action-log') or discord.utils.get(ctx.guild.channels, name='action-logging')
    embed = discord.Embed(title='Moderation Action', description=f'<@{target_id}> was {action.name} by {ctx.author.id}.', color=action.color)
    if action.type == Loggers.PURGE: embed = discord.Embed(title='Moderation Action', description=f'<@{ctx.author.id}> purged {kwargs.get("amount")} messages in <#{target_id}>.', color=action.color)
    acti_id = gen_id(8, 'action')

    query = acti.insert().values([{
        "action_id": acti_id,
        "executor_id": ctx.author.id,
        "target_id": target_id,
        "type": action.code,
        "reason": reason
    }])

    embed.add_field(name='Time (UTC)', value=datetime.utcnow().strftime("%b %d %Y, %I:%M:%S %p"))
    embed.add_field(name='Reason', value=reason)
    embed.set_footer(text='Action ID: ' + str(acti_id))
    db.execute(query)
    await logging_channel.send(embed=embed)

async def warn_user(ctx:commands.Context, target:discord.Member, reason:str):
    lt = LoggingType(Loggers.WARNING, 'warned', 0x12a805)
    await log_infraction(lt, ctx, target, reason=reason)

    try: await target.send('**You have been warned in Pokechu for the following reason**:\n' + f'`{reason}`')
    except: pass

async def mute_user(ctx:commands.Context, target:discord.Member, reason:str):
    muted_role = discord.utils.get(ctx.guild.roles, name='Muted')
    await target.add_roles(muted_role, reason=reason)

    lt = LoggingType(Loggers.MUTE, 'muted', 0xe2e200)
    await log_infraction(lt, ctx, target, reason=reason)

    try: await target.send('**You have been warned in Pokechu for the following reason**:\n' + f'`{reason}`')
    except: pass

async def kick_user(ctx:commands.Context, target:discord.Member, reason:str):
    await ctx.guild.kick(target, reason=reason)

    lt = LoggingType(Loggers.KICK, 'kicked', 0xe28400)
    await log_infraction(lt, ctx, target, reason=reason)

    try: await target.send('**You have been warned in Pokechu for the following reason**:\n' + f'`{reason}`')
    except: pass

async def ban_user(ctx:commands.Context, target:discord.Member, reason:str):
    await ctx.guild.ban(target, reason=reason)
    
    lt = LoggingType(Loggers.BAN, 'banned', 0xc10300)
    await log_infraction(lt, ctx, target, reason=reason)

    try: await target.send('**You have been warned in Pokechu for the following reason**:\n' + f'`{reason}`')
    except: pass

async def purge_messages(ctx : commands.Context, amount_to_delete : int, *, reason=None): # returns List[discord.message]
    deleted = await ctx.channel.purge(limit=amount_to_delete, bulk=True)

    lt = LoggingType(Loggers.PURGE, 'purged', 0x12a805)
    await log_action(lt, ctx, ctx.channel.id, reason=reason, amount=amount_to_delete)

    return deleted