from random import randint
from sqlalchemy import Table
from utilities import db_handler
from math import sqrt
import discord.ext.commands
import discord.utils
db = db_handler.db
xp : Table = db_handler.xp

'''
✓ Generate XP
✓ Calculate level
✓ Check user ID
✓ Get user XP
✓ Get user level
✓ Get user messages
✓ Get user block time
✓ Add user XP
✓ Add user messages
✓ Add user levels
✓ Set user XP
✓ Set user messages
✓ Set user levels (should also change their xp)
✓ Set user block time
'''

# Non GAS functions
def generate_xp(message_content : str) -> int:
    length = len(message_content)
    if length <= 15: XP = randint(5, 10)
    elif 15 < length <= 25: XP = randint(10, 15)
    elif 25 < length <= 35: XP = randint(15, 20)
    elif 35 < length <= 45: XP = randint(20, 25)
    else: XP = randint(25, 30)
    return XP

def calculate_level(xp) -> int: return int(((sqrt(625+(100*xp))-25)/50)+1)

def check_user_id(user_id : int or str) -> bool:
    user_id = str(user_id)
    query = xp.select().where(xp.c.user_id==user_id)
    res = db.execute(query)
    if res.rowcount > 0: return True
    else: return False

# Begin GAS functions
def get_user_xp(user_id : int or str) -> int:
    user_id = str(user_id)
    query = xp.select().where(xp.c.user_id==user_id)
    return db.execute(query).fetchone()[4]

def get_user_level(user_id : int or str) -> int:
    user_id = str(user_id)
    query = xp.select().where(xp.c.user_id==user_id)
    return db.execute(query).fetchone()[3]

def get_user_messages(user_id : int or str) -> int:
    user_id = str(user_id)
    query = xp.select().where(xp.c.user_id==user_id)
    return db.execute(query).fetchone()[2]

def get_user_block_time(user_id : int or str) -> int:
    user_id = str(user_id)
    query = xp.select().where(xp.c.user_id==user_id)
    return db.execute(query).fetchone()[1]

def add_user_xp(user_id : int or str, xp_to_add : int): 
    user_id = str(user_id)
    query = xp.update().where(xp.c.user_id==user_id).values(xp=get_user_xp(user_id)+xp_to_add)
    db.execute(query)

def add_user_levels(user_id : int or str, leve_to_add : int): 
    user_id = str(user_id)
    query = xp.update().where(xp.c.user_id==user_id).values(level=get_user_level(user_id)+leve_to_add)
    db.execute(query)

def add_user_messages(user_id : int or str, messages_to_add : int): 
    user_id = str(user_id)
    query = xp.update().where(xp.c.user_id==user_id).values(messages=get_user_messages(user_id)+messages_to_add)
    db.execute(query)

def set_user_xp(user_id : int or str, new_value : int): 
    user_id = str(user_id)
    query = xp.update().where(xp.c.user_id==user_id).values(xp=new_value)
    db.execute(query)

def set_user_messages(user_id : int or str, new_value : int): 
    user_id = str(user_id)
    query = xp.update().where(xp.c.user_id==user_id).values(messages=new_value)
    db.execute(query)
    
def set_user_level(user_id : int or str, new_value : int): 
    user_id = str(user_id)
    query = xp.update().where(xp.c.user_id==user_id).values(level=new_value)
    db.execute(query)

def set_user_block_time(user_id : int or str, new_value : int): 
    user_id = str(user_id)
    query = xp.update().where(xp.c.user_id==user_id).values(xp_block_until=new_value)
    db.execute(query)